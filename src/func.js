const getSum = (str1, str2) => {
  if (
    isNaN(str1) ||
    Array.isArray(str1) ||
    isNaN(str2) ||
    Array.isArray(str2)
  ) {
    return false;
  }
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = 0;
  let com = 0;

  listOfPosts.forEach(({ author, comments }) => {
    if (author) {
      post += author === authorName;
    }

    if (comments) {
      com += comments.filter(({ author }) => author === authorName).length;
    }
  });

  return `Post:${post},comments:${com}`;
};

const tickets = (people) => {
  let dollars_25 = 0,
    dollars_50 = 0;

  for (let i in people) {
    if (people[i] === 25) {
      dollars_25++;
    } else if (people[i] === 50) {
      dollars_25--;
      dollars_50++;
    } else if (people[i] === 100) {
      if (dollars_50 === 0 && dollars_25 >= 3) {
        dollars_25 -= 3;
      } else {
        dollars_25--;
        dollars_50--;
      }
    }

    if (dollars_25 < 0 || dollars_50 < 0) {
      return 'NO';
    }
  }
  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
